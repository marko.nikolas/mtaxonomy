<?php
/**
 * Plugin Name: MTaxonomy
 * Description: Register custom taxonomies.
 * Version:     0.0.1
 * Author:      Marko Nikolaš
 * Author URI:  https://gitlab.com/marko.nikolas
 * Text Domain: mtaxonomy
 * Domain Path: /languages
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package MTaxonomy.
 */

namespace MTaxonomy;

if ( ! defined( 'PLUGIN_DIR' ) ) {
	define( 'PLUGIN_DIR', plugin_dir_url( __FILE__ ) );
}

/**
 * Include plugin scripts.
 */
require_once 'includes/enqueue.php';

/**
 * Add custom taxonomies.
 */
require_once 'includes/taxonomies.php';
