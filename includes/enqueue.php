<?php
/**
 * Enqueue plugin assets.
 *
 * @package MTaxonomy
 */

namespace MTaxonomy;

/**
 * Enqueue stylesheets.
 */
function stylesheets_enqueue() {
	wp_enqueue_style( 'mtaxonomy', PLUGIN_DIR . 'style.css', '1.0', 'all' );
}

add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\stylesheets_enqueue' );
