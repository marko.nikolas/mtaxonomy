<?php
/**
 * Register custom taxonomies
 *
 * @package MTaxonomy
 */

/**
 * Register Study Type CT for Evidence CPT.
 */
function register_taxonomy_study_type() {
	$labels = array(
		'name'              => _x( 'Study types', 'taxonomy general name' ),
		'singular_name'     => _x( 'Study type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Study types' ),
		'all_items'         => __( 'All Study types' ),
		'parent_item'       => __( 'Parent Study type' ),
		'parent_item_colon' => __( 'Parent Study type:' ),
		'edit_item'         => __( 'Edit Study type' ),
		'update_item'       => __( 'Update Study type' ),
		'add_new_item'      => __( 'Add New Study type' ),
		'new_item_name'     => __( 'New Study type Name' ),
		'menu_name'         => __( 'Study type' ),
	);
	$args   = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'study-type' ),
	);
	register_taxonomy( 'study_type', array( 'evidence' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_study_type' );

/**
 * Register Area of research CT for Evidence CPT.
 */
function register_taxonomy_aor() {
	$labels = array(
		'name'              => _x( 'Area of research', 'taxonomy general name' ),
		'singular_name'     => _x( 'Area of research', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Area of research' ),
		'all_items'         => __( 'All Area of research' ),
		'parent_item'       => __( 'Parent Area of research' ),
		'parent_item_colon' => __( 'Parent Area of research:' ),
		'edit_item'         => __( 'Edit Area of research' ),
		'update_item'       => __( 'Update Area of research' ),
		'add_new_item'      => __( 'Add New Area of research' ),
		'new_item_name'     => __( 'New Area of research Name' ),
		'menu_name'         => __( 'Area of research' ),
	);
	$args   = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'area-of-research' ),
	);
	register_taxonomy( 'area_of_research', array( 'evidence' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_aor' );

/**
 * Register Routes of administration CT for Evidence CPT.
 */
function register_taxonomy_roa() {
	$labels = array(
		'name'              => _x( 'Route of administration', 'taxonomy general name' ),
		'singular_name'     => _x( 'Route of administration', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Route of administration' ),
		'all_items'         => __( 'All Route of administration' ),
		'parent_item'       => __( 'Parent Route of administration' ),
		'parent_item_colon' => __( 'Parent Route of administration:' ),
		'edit_item'         => __( 'Edit Route of administration' ),
		'update_item'       => __( 'Update Route of administration' ),
		'add_new_item'      => __( 'Add New Route of administration' ),
		'new_item_name'     => __( 'New Route of administration Name' ),
		'menu_name'         => __( 'Route of administration' ),
	);
	$args   = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'route-of-administration' ),
	);
	register_taxonomy( 'routes_of_administration', array( 'evidence' ), $args );
}
add_action( 'init', __NAMESPACE__ . '\register_taxonomy_roa' );
